<?php
/**
 * Created by PhpStorm.
 * User: kwafo
 * Date: 2/5/2018
 * Time: 10:28 AM
 */

if(!function_exists('auditlog'))
{
    function auditlog($userid,$event_type,$description)
    {
        $ci =& get_instance();
        $data = array(
            'userid' => $userid,
            'event_type ' => $event_type,
            'description'=>$description,
            'date'=>date("Y-m-d"),
            'time'=>date("h:i:s")

        );
        $ci->db->insert('audit_log', $data);


    }
}
