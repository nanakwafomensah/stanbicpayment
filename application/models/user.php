<?php
Class User extends CI_Model
{
    function login($username, $password)
    {
        $status="A";
        // $delete_flag="N";

        $this -> db -> select('petrausers.id,petrausers.useremail,petrausers.logintoken, petrausers.usersfullname,petrausers.role,branch.id as branchid,BranchName,BranchSortCode,SuspenseAccountNumber');
        $this -> db -> from('petrausers');
        $this -> db->  join('branch', 'petrausers.userbranch = branch.id', 'left');
        $this -> db -> where('petrausers.useremail', $username);
        $this -> db -> where('petrausers.logintoken', $password);
        $this -> db -> where('petrausers.status', $status);


        $this -> db -> limit(1);

        $query = $this -> db -> get();

        if($query -> num_rows() == 1)
        {
            return $query->result();
        }
        else
        {
            return false;
        }
    }
}
?>