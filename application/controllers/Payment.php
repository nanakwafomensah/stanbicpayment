<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->library('session');
//        if(empty($this->session->userdata("logged_in")))
//        {
//            redirect(base_url(),'refresh');
//        }
        ini_set('max_execution_time', 0);
        ini_set('memory_limit','2048M');
    }
   
    public function index()
    {

       $records['fullName']=$this->session->flashdata('full_name');
       $records['phoneNumber']=$this->session->flashdata('phoneNumber');
       $records['pensionNumber']=$this->session->flashdata('pensionNumber');
       $records['payeesname']=$this->session->flashdata('payeesname');
       $records['payeescontact']=$this->session->flashdata('payeescontact');
       $records['pensionYear']=$this->session->flashdata('pensionYear');
       $records['pensionMonth']=$this->session->flashdata('pensionMonth');
        //MOBILE PAYMENT MESSAGE
       $records['paymentMessage']=$this->session->flashdata('paymentMessage');
        
       $this->load->view('payment_view',$records);
    }
    public function callvisa_payment(){
        $amount=$this->input->post("amount");
        $tranid=urlencode(rand());
        echo "http://184.168.147.165:81/api/stanbic/visa/visapayment/?amount=$amount&tranid=$tranid" ;

    }
    public function callmastercard_payment(){
        $amount=$this->input->post("amount");
        $tranid=urlencode(rand());
        echo "http://184.168.147.165:81/api/stanbic/mastercard/mastercardpayment/?amount=$amount&tranid=$tranid" ;

    }
    public function validate(){
        $pensionNumber = trim($this->input->post("pensionNumber"));
        $fullName= verifyCustomer($pensionNumber);
         echo $fullName;
    }
    public function validatebusiness(){
        $transid= trim($this->input->post("transid"));
        $amount= verifybusiness($transid);
        echo $amount;
    }

    public function storecash(){
        $session_data = $this->session->userdata('logged_in');
        $user=$session_data['username'];

        //call api for successstatus field in database

        $pensionid = trim($this->input->post("pensionid"));
        $payersname = trim($this->input->post("payersname"));
        $payersnumber = trim($this->input->post("payersnumber"));
        $amount = trim($this->input->post("amount"));
        $date=date('Y-m-d');
        $time=date('H:i:s');
        $transid=trim($this->input->post("transid"));
        $paymentmode=trim($this->input->post("paymentmode"));
        $checknumber=trim($this->input->post("checknumber"));
        $branchsortcode=trim($this->input->post("branchsortcode"));
        $suspenseaccount=trim($this->input->post("suspenseaccount"));
        $category=trim($this->input->post("category"));
        $branch=trim($this->input->post("branch"));
        $contr_name=trim($this->input->post("contr_name"));
        $status='P';
        $contr_duration=explode("-",trim($this->input->post("contr_duration")));
        $contr_month=$contr_duration[1];
        $contr_year=$contr_duration[0];


        $data = array(
            'pensionid' => $pensionid,
            'payeesname' =>$payersname,
            'payeescontact'=>$payersnumber,
            'amount'=>$amount,
            'datetrans'=>$date,
            'timetrans'=>$time,
            'transid'=>$transid,
            'paymentmode'=>$paymentmode,
            'chequenumber'=>$checknumber,
            'branchsortcode'=>$branchsortcode,
            'category'=>$category,
            'branch'=>$branch,
            'status'=>$status,
            'contr_month'=>$contr_month,
            'contr_year'=>$contr_year,
            'contr_name'=>$contr_name,
            'suspenseaccount'=>$suspenseaccount,
            'userid'=>$user,



        );
       if($this->db->insert('trans', $data)){
           echo "success|$transid";
       }else{
           echo "error";
       }

      
    }
    public function storecheck(){
        $session_data = $this->session->userdata('logged_in');
        $user=$session_data['username'];


        $pensionid = trim($this->input->post("pensionid"));
        $payersname = trim($this->input->post("payersname"));
        $payersnumber = trim($this->input->post("payersnumber"));
        $amount = trim($this->input->post("amount"));
        $date=date('Y-m-d');
        $time=date('H:i:s');
        $transid=trim($this->input->post("transid"));

        $paymentmode=trim($this->input->post("paymentmode"));
        $checknumber=trim($this->input->post("checknumber"));
        $banksortcode=trim($this->input->post("banksortcode"));
        $suspenseaccount=trim($this->input->post("suspenseaccount"));
        $category=trim($this->input->post("category"));
        $branch=trim($this->input->post("branch"));
        $contr_name=trim($this->input->post("contr_name"));
        $status='U';
        $contr_duration=explode("-",trim($this->input->post("contr_duration")));
        $contr_month=$contr_duration[1];
        $contr_year=$contr_duration[0];

        $data = array(
            'pensionid' => $pensionid,
            'payeesname' =>$payersname,
            'payeescontact'=>$payersnumber,
            'amount'=>$amount,
            'datetrans'=>$date,
            'timetrans'=>$time,
            'transid'=>$transid,
            'paymentmode'=>$paymentmode,
            'chequenumber'=>$checknumber,
            'banksortcode'=>$banksortcode,
            'category'=>$category,
            'branch'=>$branch,
            'status'=>$status,
            'contr_month'=>$contr_month,
            'contr_year'=>$contr_year,
            'contr_name'=>$contr_name,
            'suspenseaccount'=>$suspenseaccount,
            'userid'=>$user,



        );
        //echo $data;
        if($this->db->insert('trans', $data)){
            echo "success|$transid";
        }else{
            echo "error";
        }


    }
    public function storetransfer(){
        $pensionid = trim($this->input->post("pensionid"));
        $payersname = trim($this->input->post("payersname"));
        $payersnumber = trim($this->input->post("payersnumber"));
        $amount = trim($this->input->post("amount"));
        $date=date('Y-m-d');
        $time=date('H:i:s');
        $transid=trim($this->input->post("transid"));

        $paymentmode=trim($this->input->post("paymentmode"));
        $checknumber=trim($this->input->post("checknumber"));
        $branchsortcode=trim($this->input->post("branchsortcode"));
        $suspenseaccount=trim($this->input->post("suspenseaccount"));
        $category=trim($this->input->post("category"));
        $branch=trim($this->input->post("branch"));
        $contr_name=trim($this->input->post("contr_name"));
        $status='P';
        $contr_duration=explode("-",trim($this->input->post("contr_duration")));
        $contr_month=$contr_duration[1];
        $contr_year=$contr_duration[0];


        $data = array(
            'pensionid' => $pensionid,
            'payeesname' =>$payersname,
            'payeescontact'=>$payersnumber,
            'amount'=>$amount,
            'datetrans'=>$date,
            'timetrans'=>$time,
            'transid'=>$transid,
            'paymentmode'=>$paymentmode,
            'chequenumber'=>$checknumber,
            'branchsortcode'=>$branchsortcode,
            'category'=>$category,
            'branch'=>$branch,
            'status'=>$status,
            'contr_month'=>$contr_month,
            'contr_year'=>$contr_year,
            'contr_name'=>$contr_name,
            'suspenseaccount'=>$suspenseaccount


        );
        if($this->db->insert('trans', $data)){
            echo "success|$transid";
        }else{
            echo "error";
        }


    }
    
    
    
    public function updatecashbusiness(){
      $transid = trim($this->input->post("transid"));
      $contr_duration=explode("-",trim($this->input->post("contr_duration")));
      $contr_month=$contr_duration[1];
      $contr_year=$contr_duration[0];
      
      $data = array(
          'status' => 'P',
          'payeesname' =>$this->input->post("payersname"),
          'payeescontact'=>$this->input->post("payersnumber"),
          'paymentmode'=>$this->input->post("paymentmode"),
          'chequenumber'=>$this->input->post("checknumber"),
          'branchsortcode'=>$this->input->post("branchsortcode"),
          'category'=>$this->input->post("category"),
          'branch'=>$this->input->post("branch"),
          'contr_month'=>$contr_month,
          'contr_year'=>$contr_year


      );
      $this->db->where('transid', $transid);

      if($this->db->update('trans',$data)){
          echo "success|$transid";
      }else{
          echo "error";
      }
  }


    public function updatecash(){
        
        $id=$this->input->post("id_cash");


        $data = array(
            'pensionid' => $this->input->post("pensionid_cash"),
            'payeesname' => $this->input->post("payeesname_cash"),
            'payeescontact' => $this->input->post("payeescontact_cash"),
            'amount' => $this->input->post("amount_cash")


        );
        $this->db->where('id', $id);
        $this->db->update('trans',$data);
        //Go to private area
        redirect('individualCash', 'refresh');
    }
    public function updatecheck(){

        $id=$this->input->post("id_cheq");
        $data = array(
            'pensionid' => $this->input->post("pensionid_cheq"),
            'payeesname' => $this->input->post("payeesname_cheq"),
            'payeescontact' => $this->input->post("payeescontact_cheq"),
            'amount' => $this->input->post("amount_cheq"),
            'branchsortcode' => $this->input->post("bank_cheq"),
            'chequenumber' => $this->input->post("cheqnumber_cheq"),


        );
        $this->db->where('id', $id);
        $this->db->update('trans',$data);
        redirect('individualCheck', 'refresh');
    }

    public function clientpay(){

        $pensionYear= urlencode($this->input->post("pensionYear"));
        $pensionMonth= urlencode($this->input->post("pensionMonth"));
        $contr_name= urlencode($this->input->post("contr_name"));
        $telNumber= urlencode($this->input->post("number"));
        $amount= urlencode($this->input->post("amount"));
        $pensionid= urlencode($this->input->post("pensionid"));
        $payeesname= urlencode($this->input->post("payeesname"));
        $payeescontact= urlencode($this->input->post("payeescontact"));
        $nw= urlencode($this->input->post("network"));
        $transid=urlencode(rand());
        $date=urlencode(date('Y-m-d H:i:s'));
        $url="http://184.168.147.165:81/stanbic/debit.php?number=$telNumber&amount=$amount&transid=$transid&nw=$nw&date=$date&pensionid=$pensionid&payeesname=$payeesname&payeescontact=$payeescontact&pensionYear=$pensionYear&pensionMonth=$pensionMonth&contr_name=$contr_name";
        file_get_contents($url);
        $home=base_url();
        $link='<strong><a href="'.$home.'">Go to Home</a></strong>';
        $this->session->set_flashdata('paymentMessage', "You would receive a prompt on your phone for confirmation Shortly \n Feel free to go back to the home page .$link");
        redirect('paypage');

    }

}
