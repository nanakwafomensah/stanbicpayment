<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->library('session');
//		if(empty($this->session->userdata("logged_in")))
//		{
//			redirect(base_url(),'refresh');
//		}
		ini_set('max_execution_time', 0);
		ini_set('memory_limit','2048M');
	}
	
	public function index()
	{
		$this->load->view('index_view');
	}


	public function getdetails()
	{
		$pensionNumber = trim($this->input->post("pensionNumber"));
		$pensionduration = explode("-",trim($this->input->post("contributerMonth")));
		$pensionYear = $pensionduration[0];
		$pensionMonth = $pensionduration[1];
//		die;
        $phoneNumber = trim($this->input->post("phoneNumber"));
		$payeesname=trim($this->input->post("payeesname"));
			$payeescontact=trim($this->input->post("phoneNumber"));
		//helper call

		$fullName= verifyCustomer($pensionNumber);

		if($fullName=='error'){
			$this->session->set_flashdata('errormessage', "Pension ID could not be validated");
			redirect("home");
		}else{
			// Set flash data
			$this->session->set_flashdata('full_name', $fullName);
			$this->session->set_flashdata('phoneNumber', $phoneNumber);
			$this->session->set_flashdata('pensionNumber', $pensionNumber);
			$this->session->set_flashdata('payeesname', $payeesname);
			$this->session->set_flashdata('payeescontact', $payeescontact);

			$this->session->set_flashdata('pensionYear', $pensionYear);
			$this->session->set_flashdata('pensionMonth', $pensionMonth);
//		// After that you need to used redirect function instead of load view such as
			redirect("paypage");
		}



	}
}
