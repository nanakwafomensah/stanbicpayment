<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->library('session');
        if(empty($this->session->userdata("logged_in")))
        {
            redirect(base_url(),'refresh');
        }
    }

    function logout()
    {
        $this->session->unset_userdata('logged_in');
        session_destroy();
        redirect('login', 'refresh');
    }

    public function change_password()
    {
        $password=$this->input->post("oldpass");
        $new_pass=$this->input->post("newpass");
        $new_pass_confirm=$this->input->post("newpassconf");
        $useremail=$this->input->post("useremail");

        $logintoken=hash('sha512',$password);
        $result=$this->validate_old_password($logintoken,$useremail);
       if($result=='1')
        {
            if($new_pass==$new_pass_confirm)
            {

                $data = array('logintoken' => hash('sha512', $new_pass) );
                $this->db->set($data);
                $this->db->where('useremail',$useremail);
                $this->db->update("petrausers",$data);

                echo $result;
            }
            else
            {
                //return 2;
                $result=2;
                echo $result;
            }

        }
        else{

            echo $result;
        }

    }
    public function validate_old_password($logintoken,$useremail){ 
        $query=$this->db->query("select logintoken from petrausers where logintoken='$logintoken'  and useremail='$useremail' ");
      $result=$query->num_rows();
      if($result >=1)
        {return 1;}else { return 0; }
    }

}
