<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class VerifyLogin extends CI_Controller {


    function __construct()
    {
        parent::__construct();
        $this->load->model('user','',TRUE);
        $this->load->helper('form');
        $this->load->helper('url');
//        if(empty($this->session->userdata("logged_in")))
//        {
//            redirect(base_url(),'refresh');
//        }
    }

    function index()
    {

        //This method will have the credentials validation
        $this->load->library('form_validation');

        $this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|callback_check_database');

        if($this->form_validation->run() == FALSE)
        {
            //Field validation failed.  User redirected to login page
            $this->load->view('admin/index');
        }
        else
        {
            //Go to private area
            redirect('individualCash', 'refresh');
        }

    }

    function check_database($password)
    {
        //Field validation succeeded.  Validate against database
        $username = $this->input->post('username');

        //query the database
        $g=hash('sha512',$password);
        $result = $this->user->login($username, $g);

        if($result)
        {
            $sess_array = array();
            foreach($result as $row)
            {
                $sess_array = array(
                    'id' => $row->id,
                    'username' => $row->usersfullname,
                    'role'=>$row->role,
                    'useremail'=>$row->useremail,
                    'BranchName'=>$row->BranchName,
                    'BranchSortCode'=>$row->BranchSortCode,
                    'SuspenseAccountNumber'=>$row->SuspenseAccountNumber
                );
                $this->session->set_userdata('logged_in', $sess_array);
            }
            return TRUE;
        }
        else
        {
            $this->form_validation->set_message('check_database', 'Invalid username or password');
            return false;
        }
    }
}
?>