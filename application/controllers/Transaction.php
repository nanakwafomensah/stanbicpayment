<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Transaction extends CI_Controller {


    function __construct()
    {
        parent::__construct();
        $this->load->model('user','',TRUE);
        $this->load->helper('form');
        $this->load->helper('url');
        if(empty($this->session->userdata("logged_in")))
        {
            redirect(base_url(),'refresh');
        }

    }

 public function individualCash()
    {
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');

            $records['usersfullname'] = $session_data['username'];
            $records['user_id_s'] = $session_data['id'];
            $records['role'] = $session_data['role'];
            $records['useremail'] = $session_data['useremail'];
            $records['BranchName'] = $session_data['BranchName'];
            $records['BranchSortCode'] = $session_data['BranchSortCode'];
            $records['SuspenseAccountNumber'] = $session_data['SuspenseAccountNumber'];
            ////////////////////////////////////////////////////////////////////////
            $this->load->view('admin/individualCash_view',$records);
        }else{
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }


    }
 public function individualCheck()
    {
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');

            $records['usersfullname'] = $session_data['username'];
            $records['user_id_s'] = $session_data['id'];
            $records['role'] = $session_data['role'];
            $records['useremail'] = $session_data['useremail'];
            $records['BranchName'] = $session_data['BranchName'];
            $records['BranchSortCode'] = $session_data['BranchSortCode'];
            $records['SuspenseAccountNumber'] = $session_data['SuspenseAccountNumber'];


            ////////////////////////////////////////////////////////////////////////
            $query = $this->db->query("select sortcode,bankname from banks where status ='A'");
            $records['banks'] = $query->result();
            $this->load->view('admin/individualCheck_view',$records);
        }else{
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }


    }
 public function businessCash()
    {

        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');
   if(isset($session_data['username'])){
       $records['usersfullname'] = $session_data['username'];
       $records['user_id_s'] = $session_data['id'];
       $records['role'] = $session_data['role'];
       $records['useremail'] = $session_data['useremail'];
       $records['BranchName'] = $session_data['BranchName'];
       $records['BranchSortCode'] = $session_data['BranchSortCode'];
       $records['SuspenseAccountNumber'] = $session_data['SuspenseAccountNumber'];

       ////////////////////////////////////////////////////////////////////////
       $this->load->view('admin/businessCash_view',$records);
   }else{
       redirect('login', 'refresh');
   }

        }else{
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }

    }
 public function businessCheck()
    {

        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');

            $records['usersfullname'] = $session_data['username'];
            $records['user_id_s'] = $session_data['id'];
            $records['role'] = $session_data['role'];
            $records['useremail'] = $session_data['useremail'];
            $records['BranchName'] = $session_data['BranchName'];
            $records['BranchSortCode'] = $session_data['BranchSortCode'];
            $records['SuspenseAccountNumber'] = $session_data['SuspenseAccountNumber'];

            $query = $this->db->query("select sortcode,bankname from banks where status ='A'");
            $records['banks'] = $query->result();

            ////////////////////////////////////////////////////////////////////////
            $this->load->view('admin/businessCheck_view',$records);
        }else{
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }

    }

    public function individualtransaction(){
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');

            $records['usersfullname'] = $session_data['username'];
            $records['user_id_s'] = $session_data['id'];
            $records['role'] = $session_data['role'];
            $records['useremail'] = $session_data['useremail'];
            $records['BranchName'] = $session_data['BranchName'];
            $records['BranchSortCode'] = $session_data['BranchSortCode'];
            $records['SuspenseAccountNumber'] = $session_data['SuspenseAccountNumber'];

            ////////////////////////////////////////////////////////////////////////
            $query = $this->db->query("select * from trans where paymentmode ='INDIVIDUAL CASH' or paymentmode ='INDIVIDUAL CHEQUE' ");
            $records['records'] = $query->result();

            $query = $this->db->query("select sortcode,bankname from banks where status ='A'");
            $records['banks'] = $query->result();
            
            $this->load->view('admin/individual_trans_view',$records);
        }else{
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
    } 
    public function businesstransaction(){
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');

            $records['usersfullname'] = $session_data['username'];
            $records['user_id_s'] = $session_data['id'];
            $records['role'] = $session_data['role'];
            $records['useremail'] = $session_data['useremail'];
            $records['BranchName'] = $session_data['BranchName'];
            $records['BranchSortCode'] = $session_data['BranchSortCode'];
            $records['SuspenseAccountNumber'] = $session_data['SuspenseAccountNumber'];

            ////////////////////////////////////////////////////////////////////////
            $query = $this->db->query("select * from trans where paymentmode ='BUSINESS CASH' or paymentmode ='BUSINESS CHEQUE' ");
            $records['records'] = $query->result();

            $query = $this->db->query("select sortcode,bankname from banks where status ='A'");
            $records['banks'] = $query->result();
            
            $this->load->view('admin/business_trans_view',$records);
        }else{
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
    }

    public function individualTransfer(){
        if($this->session->userdata('logged_in'))
        {
            $session_data = $this->session->userdata('logged_in');

            $records['usersfullname'] = $session_data['username'];
            $records['user_id_s'] = $session_data['id'];
            $records['role'] = $session_data['role'];
            $records['useremail'] = $session_data['useremail'];
            $records['BranchName'] = $session_data['BranchName'];
            $records['BranchSortCode'] = $session_data['BranchSortCode'];
            $records['SuspenseAccountNumber'] = $session_data['SuspenseAccountNumber'];

            ////////////////////////////////////////////////////////////////////////
            $query = $this->db->query("select * from trans where paymentmode ='BUSINESS CASH' or paymentmode ='BUSINESS CHEQUE' ");
            $records['records'] = $query->result();

            $query = $this->db->query("select sortcode,bankname from banks where status ='A'");
            $records['banks'] = $query->result();

            $this->load->view('admin/individualTransfer_view',$records);
        }else{
            //If no session, redirect to login page
            redirect('login', 'refresh');
        }
    }
 public function businessTransfer(){
     if($this->session->userdata('logged_in'))
     {
         $session_data = $this->session->userdata('logged_in');

         $records['usersfullname'] = $session_data['username'];
         $records['user_id_s'] = $session_data['id'];
         $records['role'] = $session_data['role'];
         $records['useremail'] = $session_data['useremail'];
         $records['BranchName'] = $session_data['BranchName'];
         $records['BranchSortCode'] = $session_data['BranchSortCode'];
         $records['SuspenseAccountNumber'] = $session_data['SuspenseAccountNumber'];

         ////////////////////////////////////////////////////////////////////////
         $query = $this->db->query("select * from trans where paymentmode ='BUSINESS CASH' or paymentmode ='BUSINESS CHEQUE' ");
         $records['records'] = $query->result();

         $query = $this->db->query("select sortcode,bankname from banks where status ='A'");
         $records['banks'] = $query->result();

         $this->load->view('admin/businessTransfer_view',$records);
     }else{
         //If no session, redirect to login page
         redirect('login', 'refresh');
     }
 }


}
?>