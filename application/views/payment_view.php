<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title>Stanbic-Pension Contribution.</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link rel="apple-touch-icon" href="pages/ico/60.png">
    <link rel="apple-touch-icon" sizes="76x76" href="pages/ico/76.png">
    <link rel="apple-touch-icon" sizes="120x120" href="pages/ico/120.png">
    <link rel="apple-touch-icon" sizes="152x152" href="pages/ico/152.png">
    <link rel="icon" type="image/x-icon" href="assets/img/favicon.ico" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <link href="assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/boostrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/jquery-scrollbar/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="assets/plugins/bootstrap-select2/select2.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="assets/plugins/switchery/css/switchery.min.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="pages/css/pages-icons.css" rel="stylesheet" type="text/css">
    <link class="main-stylesheet" href="pages/css/pages.css" rel="stylesheet" type="text/css" />
    <!--[if lte IE 9]>
    <link href="pages/css/ie9.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="assets/parsley/css/parsley.css" rel="stylesheet" type="text/css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <![endif]-->
    <script type="text/javascript">
        window.onload = function()
        {
            // fix for windows 8
            if (navigator.appVersion.indexOf("Windows NT 6.2") != -1)
                document.head.innerHTML += '<link rel="stylesheet" type="text/css" href="pages/css/windows.chrome.fix.css" />'
        }
    </script>
    <style>
        div.accordion {
            background-color: #eee;
            color: #444;
            cursor: pointer;
            padding: 18px;
            width: 100%;
            border: none;
            text-align: left;
            outline: none;
            font-size: 15px;
            transition: 0.4s;
        }

        div.accordion.active, button.accordion:hover {
            background-color: #ddd;
        }

        div.panel {
            padding: 0 18px;
            display: none;
            background-color: white;
        }
    </style>
</head>
<body class="fixed-header" style = "background-color: #fff">
<div height=" 15px" style="background-color: #26428b; color: #26428b">f </div>
<div class="register-container full-height sm-p-t-30" style ="margin-top: -30px">
    <div class="container-sm-height full-height">
        <div class="row row-sm-height">
            <div class="col-sm-12 col-sm-height col-middle">
                <img src="assets/img/logo.png" alt="logo" data-src="assets/img/logo.png" data-src-retina="assets/img/logo.png" width="311" height="76">

                  <?php
                    if(isset($paymentMessage)){
                        echo '<div class="alert alert-success" role="alert">
  <h4 class="alert-heading">Well done!</h4>
  <p>'.$paymentMessage.'</p>
  <hr>
  <p class="mb-0">Cheers</p>
</div>';
                    }else{
                        echo '<h3>Please Confirm Contributors Details &amp Proceed With Payment</h3>';
                        echo '  <form id="myformpayment"  data-parsley-validate="">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group form-group-default">
                                <label>Contributor\'s Name </label>
                                <input type="text" name="contr_name" id="contr_name"  class="form-control" value = "'.  $fullName.'" readonly="true" required  style=" color: #26428b ">
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group form-group-default">
                                <label>Phone Number</label>
                                <input type="text" name="pass"  class="form-control" required  style="color: #26428b; " readonly="true" value = "'. $phoneNumber.'">
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group form-group-default">
                                <label>Pension ID#</label>
                                <input type="text" name="pensionid_text" id="pensionid_text"  class="form-control" required  readonly="true" style="color: #26428b; " value="'.$pensionNumber.'">
                            </div>
                        </div>
                    </div>
                            <div class="accordion" style="border-bottom-style: solid;border-color: #26428b"> <span class="label label-default" style="background-color:#26428b; color: white">PROCEED TO MAKE PAYMENT</span><i class="fa fa-long-arrow-right" aria-hidden="true" style="color:#26428b"></i></div>
                            <div class="panel">
                            <hr>Payment Mode
                            <div class="row">
                            <center>
                            <div class="col-sm-2">
                                       <button type="submit">
                                           <center><img src="assets/img/mtn.png" id="mtnpayment"  style="cursor: pointer;"></center>
                                       </button>
                                </div>

                                <div class="col-sm-2">
                                    <button type="submit">
                                        <center><img src="assets/img/airtel.png" id="airtelpayment"  style="cursor: pointer;"></center>
                                     </button>
                                </div>

                                <div class="col-sm-2">
                                    <button type="submit">
                                    <center><img src="assets/img/tigo.png" id="tigopayment"  style="cursor: pointer;"></center>
                                    </button>
                                </div>

                                <div class="col-sm-2">
                                    <button type="submit">
                                    <center><img src="assets/img/voda.png" id="vodafonepayment"  style="cursor: pointer;"></center>
                                    </button>
                                </div>
                            </center>
                                

                             <div class="col-sm-2">
                                    <button type="submit">
                                   <center><img src="assets/img/visa.png"   id="visaModal" style="cursor: pointer;"></center>
                                    </button>
                               </div>                               
                                <div class="col-sm-2">
                                    <button type="submit">
                                    <center><img src="assets/img/master.png"  id="masterModal" style="cursor: pointer;"></center>
                                  </button>
                                </div>
                            </div>
                            </div>

                </form>';
                    }
                  ?>


                <div class="modal fade" id="thanksVISA" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Payment Successful</h4>
                                <img src="assets/img/tick.png">
                                <p>VISA transaction is in progress .Redirecting in 5 seconds.....</p>


                            </div>
                            <div class="modal-body">

                            </div>
                            <div class="modal-footer">
                                <center><button type="button" class="btn btn-danger" style = "background-color: maroon; border-color: maroon"data-dismiss="modal" onclick="window.location.href='../index.html'"><i class="fa fa-close"> Close</i></button></center>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal fade" id="thanksmaster" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Payment Successful</h4>
                                <img src="assets/img/tick.png">
                                <p>MAster Card transaction is in progress .Redirecting in 5 seconds.....</p>


                            </div>
                            <div class="modal-body">

                            </div>
                            <div class="modal-footer">
                                <center><button type="button" class="btn btn-danger" style = "background-color: maroon; border-color: maroon"data-dismiss="modal" onclick="window.location.href='../index.html'"><i class="fa fa-close"> Close</i></button></center>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal fade" id="airtel" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Pay with Airtel Money</h4>
                                <img src="assets/img/airtel.png">
                                <form  action="Payment/clientpay" method="post" data-parsley-validate="">
                                <p>Please Enter The Airtel Number For Payment</p>
                                <input type="text" name="number" placeholder="Airtel Number" class="form-control text-center" required maxlength="10"><br>
                                <p>Please Enter The Amount Paying</p>
                                <input type="text" name="amount" placeholder="6.0" class="form-control text-center" required maxlength="10"><br>
                                <input type="hidden" id="pensionidairtel" name="pensionid"/>
                                <input type="hidden" id="contr_nameairtel" name="contr_name"/>
                                <input type="hidden" id="network" name="network" value="AIR"/>
                                <input type="hidden" id="payeesname" name="payeesname" value="<?php echo $payeesname;?>"/>
                                <input type="hidden" id="payeescontact" name="payeescontact" value="<?php echo $payeescontact;?>"/>
                                    
                                <input type="hidden" id="pensionYear" name="pensionYear" value="<?php echo $pensionYear;?>"/>
                                <input type="hidden" id="pensionMonth" name="pensionMonth" value="<?php echo $pensionMonth;?>"/>
                            </div>
                            <div class="modal-body">

                            </div>
                            <div class="modal-footer">
                                <center><button type="submit" class="btn btn-danger" style = "background-color: forestgreen; border-color: forestgreen"><i class="fa fa-money"> Pay</i></button></center>
                            </div>
                            
                            </form>
                        </div>

                    </div>
                </div>
                <div class="modal fade" id="mtn" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Pay with MTN Money</h4>
                                <img src="assets/img/mtn.png">
                                <form  action="Payment/clientpay" method="post" data-parsley-validate="">
                                <p>Please Enter The MTN Number For Payment</p>
                                <input type="text" name="number" placeholder="MTN Number" class="form-control text-center" required maxlength="10"><br>
                                <p>Please Enter The Amount Paying</p>
                                <input type="text" name="amount" placeholder="6.0" class="form-control text-center" required maxlength="10"><br>
                                <input type="hidden" id="pensionidmtn" name="pensionid"/>
                                <input type="hidden" id="contr_namemtn" name="contr_name"/>
                                <input type="hidden" id="network" name="network" value="MTN"/>
                                    <input type="hidden" id="payeesname" name="payeesname" value="<?php echo $payeesname;?>"/>
                                    <input type="hidden" id="payeescontact" name="payeescontact" value="<?php echo $payeescontact;?>"/>

                                    <input type="hidden" id="pensionYear" name="pensionYear" value="<?php echo $pensionYear;?>"/>
                                    <input type="hidden" id="pensionMonth" name="pensionMonth" value="<?php echo $pensionMonth;?>"/>
                            </div>
                            <div class="modal-body">

                            </div>
                            <div class="modal-footer">
                                <center><button type="submit" class="btn btn-danger" style = "background-color: forestgreen; border-color: forestgreen"><i class="fa fa-money"> Pay</i></button></center>
                            </div>
                            </form>
                        </div>

                    </div>
                </div>
                <div class="modal fade" id="voda" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Pay with Vodafone Cash</h4>
                                <img src="assets/img/voda.png">
                         <form  action="Payment/clientpay" method="post" data-parsley-validate="">
                                <p>Please Enter The Vodafone Number For Payment</p>
                                <input type="text" name="number" placeholder="Vodafone Number" class="form-control text-center" required maxlength="10"><br>
                                <p>Please Enter The Amount Paying</p>
                                <input type="text" name="amount" placeholder="6.0" class="form-control text-center" required maxlength="10"><br>
                                <input type="hidden" id="contr_namevodafone" name="contr_name"/>
                                <input type="hidden" id="network" name="network" value="VOD"/>
                             <input type="hidden" id="payeesname" name="payeesname" value="<?php echo $payeesname;?>"/>
                             <input type="hidden" id="payeescontact" name="payeescontact" value="<?php echo $payeescontact;?>"/>

                             <input type="hidden" id="pensionYear" name="pensionYear" value="<?php echo $pensionYear;?>"/>
                             <input type="hidden" id="pensionMonth" name="pensionMonth" value="<?php echo $pensionMonth;?>"/>
                            </div>
                            <div class="modal-body">

                            </div>
                            <div class="modal-footer">
                                <center><button type="submit" class="btn btn-danger" style = "background-color: forestgreen; border-color: forestgreen"><i class="fa fa-money"> Pay</i></button></center>
                            </div>
                            </form>
                          </div>
                        </div>

                    </div>
                </div>

                <div class="modal fade" id="tigo" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Pay with Tigo Cash</h4>
                                <img src="assets/img/tigo.png">
                             <form  action="Payment/clientpay" method="post" data-parsley-validate="">
                                <p>Please Enter The Tigo Number For Payment</p>
                                <input type="text" name="number" placeholder="Tigo Number" class="form-control text-center" required maxlength="10"><br>
                                <p>Please Enter The Amount Paying</p>
                                <input type="text" name="amount" placeholder="6.0" class="form-control text-center" required maxlength="10"><br>
                                <input type="hidden" id="pensionidtigo" name="pensionid"/>
                                <input type="hidden" id="contr_nametigo" name="contr_name"/>
                                <input type="hidden" id="network" name="network" value="TIG"/>
                                 <input type="hidden" id="payeesname" name="payeesname" value="<?php echo $payeesname;?>"/>
                                 <input type="hidden" id="payeescontact" name="payeescontact" value="<?php echo $payeescontact;?>"/>

                                 <input type="hidden" id="pensionYear" name="pensionYear" value="<?php echo $pensionYear;?>"/>
                                 <input type="hidden" id="pensionMonth" name="pensionMonth" value="<?php echo $pensionMonth;?>"/>
                            </div>
                            <div class="modal-body">

                            </div>
                            <div class="modal-footer">
                                <center><button type="submit" class="btn btn-danger" style = "background-color: forestgreen; border-color: forestgreen"><i class="fa fa-money"> Pay</i></button></center>
                            </div>
                            </form>
                        </div>

                    </div>
                </div>

                <div class="modal fade" id="VISA" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Pay with VISA</h4>
                                <img src="assets/img/visa.png">
                                <p>Please Enter The VISA Details For Payment</p>
                                <div class = "row>">
                                    <div class="col-sm-12">
                                        <div class="form-group form-group-default">
                                            <label>Enter Amount</label>
                                            <input type="text" id="visa_amount" name="visa_amount" placeholder="10.00" class="form-control" required >
                                        </div>


                                    </div>



                                </div>





                            </div>
                            <div class="modal-body">

                            </div>
                            <div class="modal-footer">
                                <center><button  id="makepaymentvisa"  type="button" class="btn btn-danger" style = "background-color: forestgreen; border-color: forestgreen"  style="cursor: pointer;"><i class="fa fa-money"> Pay</i></button></center>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="modal fade" id="master" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Pay with MasterCard</h4>
                                <img src="assets/img/master.png">
                                <p>Please Enter The MasterCard Details</p>
                                <div class = "row>">
                                    <div class="col-sm-12">
                                        <div class="form-group form-group-default">
                                            <label>Enter Amount:</label>
                                            <input type="text" id="masteramount" name="masteramount" placeholder="10.00" class="form-control" required >
                                        </div>


                                    </div>

                                </div>



                            </div>
                            <div class="modal-body">

                            </div>
                            <div class="modal-footer">
                                <center><button type="button" id="makepaymentmaster" class="btn btn-danger" style = "background-color: forestgreen; border-color: forestgreen"><i class="fa fa-money"> Pay</i></button></center>
                            </div>
                        </div>

                    </div>
                </div>


                <br><hr>
            </div>
        </div>
    </div>
</div>

<!-- START OVERLAY -->

<!-- END OVERLAY -->
<!-- BEGIN VENDOR JS -->
<script src="assets/plugins/pace/pace.min.js" type="text/javascript"></script>
<script src="assets/plugins/jquery/jquery-1.11.1.min.js" type="text/javascript"></script>
<script src="assets/plugins/modernizr.custom.js" type="text/javascript"></script>
<script src="assets/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="assets/plugins/boostrapv3/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/plugins/jquery/jquery-easy.js" type="text/javascript"></script>
<script src="assets/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script>
<script src="assets/plugins/jquery-bez/jquery.bez.min.js"></script>
<script src="assets/plugins/jquery-ios-list/jquery.ioslist.min.js" type="text/javascript"></script>
<script src="assets/plugins/jquery-actual/jquery.actual.min.js"></script>
<script src="assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js"></script>
<script type="text/javascript" src="assets/plugins/bootstrap-select2/select2.min.js"></script>
<script type="text/javascript" src="assets/plugins/classie/classie.js"></script>
<script src="assets/plugins/switchery/js/switchery.min.js" type="text/javascript"></script>
<script src="assets/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<!-- END VENDOR JS -->
<!-- BEGIN CORE TEMPLATE JS -->
<script src="pages/js/pages.min.js"></script>
<!-- END CORE TEMPLATE JS -->
<!-- BEGIN PAGE LEVEL JS -->
<script src="assets/js/scripts.js" type="text/javascript"></script>
<script src="assets/parsley/js/parsley.min.js"></script>
<!-- END PAGE LEVEL JS -->
<script>
    $(function()
    {
        $('#form-register').validate()

    })
</script>
<script>
    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].onclick = function(){
            this.classList.toggle("active");
            var panel = this.nextElementSibling;
            if (panel.style.display === "block") {
                panel.style.display = "none";
            } else {
                panel.style.display = "block";
            }
        }
    }
</script>
<script>
    $(document).on('click','#mtnpayment',function(e) {
        e.preventDefault();
        //validate textfield
        $('#myformpayment').parsley().validate();
        ////////////////////////////////////
        if($('#myformpayment').parsley().isValid()){
            var pensionid=$('#pensionid_text').val();
            var contr_name=$('#contr_name').val();
            $('#pensionidmtn').val(pensionid);
            $('#contr_namemtn').val(contr_name);

            $('#mtn').modal('show');
        }else{
   alert(0);
//            var  location = <?php //base_url()?>//;
//            window.location.href = location;

        }
    });
    $(document).on('click','#airtelpayment',function(e) {
        e.preventDefault();
        //validate textfield
        $('#myformpayment').parsley().validate();
        if($('#myformpayment').parsley().isValid()) {

            var pensionid = $('#pensionid_text').val();
            var contr_name = $('#contr_name').val();
            $('#pensionidairtel').val(pensionid);
            $('#contr_nameairtel').val(contr_name);
            $('#airtel').modal('show');
        }

    });
    $(document).on('click','#tigopayment',function(e) {
        e.preventDefault(e);
        $('#myformpayment').parsley().validate();
        if($('#myformpayment').parsley().isValid()) {
            var pensionid = $('#pensionid_text').val();
            var contr_name = $('#contr_name').val();
            $('#pensionidtigo').val(pensionid);
            $('#contr_nametigo').val(contr_name);
            $('#tigo').modal('show');
        }
    });
    $(document).on('click','#vodafonepayment',function(e) {
        e.preventDefault();
        $('#myformpayment').parsley().validate();
        if($('#myformpayment').parsley().isValid()) {
            var pensionid = $('#pensionid_text').val();
            var contr_name = $('#contr_name').val();
            $('#pensionidvodafone').val(pensionid);
            $('#contr_namevodafone').val(contr_name);
            $('#voda').modal('show');
        }

    });
    $(document).on('click','#visaModal',function(e) {
        e.preventDefault();
        $('#myformpayment').parsley().validate();
        if($('#myformpayment').parsley().isValid()) {
//            var pensionid = $('#pensionid_text').val();
//            var contr_name = $('#contr_name').val();
//            $('#pensionidvodafone').val(pensionid);
//            $('#contr_namevodafone').val(contr_name);
            $('#VISA').modal('show');
        }

    });
    $(document).on('click','#masterModal',function(e) {
        e.preventDefault();
        $('#myformpayment').parsley().validate();
        if($('#myformpayment').parsley().isValid()) {
//            var pensionid = $('#pensionid_text').val();
//            var contr_name = $('#contr_name').val();
//            $('#pensionidvodafone').val(pensionid);
//            $('#contr_namevodafone').val(contr_name);
            $('#master').modal('show');
        }

    });

    $(document).on('click','#makepaymentvisa',function(){
        $('#visa_amount').parsley().validate();
        if($('#visa_amount').parsley().isValid()){

            $.ajax({
                type:'POST',
                data:{
                    amount:$('#visa_amount').val()
                   

                },
                url:'<?php echo site_url('Payment/callvisa_payment'); ?>',
                success:function(result){
                    if(result){
                        $('#VISA').modal('hide');
                        $('#thanksVISA').modal('show');
                        setTimeout(window.location=result, 5000)

                    }




                }
            });

        }
    });
    $(document).on('click','#makepaymentmaster',function(){
//        alert($("#masteramount").val());
        $('#masteramount').parsley().validate();
        if($('#masteramount').parsley().isValid()){
            $.ajax({
                type:'POST',
                data:{
                    amount:$('#masteramount').val()
                    

                },
                url:'<?php echo site_url('Payment/callmastercard_payment'); ?>',
                success:function(result){
                    if(result){
                        $('#master').modal('hide');
                        $('#thanksmaster').modal('show');
                        setTimeout(window.location=result, 5000)

                    }




                }
            });

        }

    });


</script>

</body>
</html>