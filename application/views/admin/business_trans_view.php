<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>Stanbic-Pension Contribution.</title>
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content=""/>

    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico">
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <!-- Morris Charts CSS -->
    <link href="vendors/bower_components/morris.js/morris.css" rel="stylesheet" type="text/css"/>

    <!-- Data table CSS -->

    <link href="vendors/bower_components/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
    <link href="vendors/bower_components/datatables.net-responsive/css/responsive.dataTables.min.css" rel="stylesheet" type="text/css"/>


    <link href="vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.css" rel="stylesheet" type="text/css">
    <!-- Morris Charts CSS -->
    <link href="vendors/bower_components/morris.js/morris.css" rel="stylesheet" type="text/css"/>
    <!-- Custom CSS -->
    <link href="dist/css/style.css" rel="stylesheet" type="text/css">
    <link href="assets/parsley/css/parsley.css" rel="stylesheet" type="text/css">

</head>

<body>
<!-- Preloader -->
<div class="preloader-it">
    <div class="la-anim-1"></div>
</div>
<!-- /Preloader -->
<div class="wrapper theme-1-active pimary-color-red">
    <!-- Top Menu Items -->
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="mobile-only-brand pull-left">
            <div class="nav-header pull-left">
                <div class="logo-wrap">
                    <a href="index.html">
                        <img class="brand-img" src="assets/files/logo.png" alt="brand" height="40px" />
                    </a>
                </div>
            </div>
            <a id="toggle_nav_btn" class="toggle-left-nav-btn inline-block ml-20 pull-left" href="javascript:void(0);"><i class="zmdi zmdi-menu"></i></a>
            <a id="toggle_mobile_search" data-toggle="collapse" data-target="#search_form" class="mobile-only-view" href="javascript:void(0);"><i class="zmdi zmdi-search"></i></a>
            <a id="toggle_mobile_nav" class="mobile-only-view" href="javascript:void(0);"><i class="zmdi zmdi-more"></i></a>

        </div>
        <div id="mobile_only_nav" class="mobile-only-nav pull-right">
            <a href="login" >  <span style = "font-size: 10px"> Welcome, <?php echo $usersfullname;?></span> |  <span style = "font-size: 10px"><i class = "fa fa-lock"></i> Log Out</span></a>|<a href="#" data-toggle="modal" id="mychangepass" data-useremail="<?php echo $useremail;?>" data-target="#changePassword"><span style = "font-size: 10px; color: #1a79d6"><i class = "fa fa-edit"></i> Change Password</span></a>
        </div>
    </nav>
    <!-- /Top Menu Items -->

    <!-- Left Sidebar Menu -->
    <div class="fixed-sidebar-left">
        <ul class="nav navbar-nav side-nav nicescroll-bar">



            <li class="navigation-header">
                <span style="color:#26428b">BRANCH COLLECTION</span>
                <i class="zmdi zmdi-more"></i>
            </li>
            <li>
                <a class="active"  style="background-color:#26428b" href="javascript:void(0);" data-toggle="collapse" data-target="#ui_dr1"><div class="pull-left"><i class="fa fa-money -setup mr-20"></i><span class="right-nav-text" style = "font-size: 11px">Individual Payment</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
                <ul id="ui_dr1" class="collapse collapse-level-1 two-col-list">


                    <li>
                        <a href="individualCash" style = "font-size: 11px">Cash</a>
                    </li>

                    <li>
                        <a href="individualCheck" style = "font-size: 11px">Cheque</a>
                    </li>
                    <li>
                        <a href="individualTransfer" style = "font-size: 11px">Transfer</a>
                    </li>
                    <li>
                        <a href="individualtrans" style = "font-size: 11px">All Transactions</a>
                    </li>

                </ul>
            </li>
            <li>
                <a href="javascript:void(0);" data-toggle="collapse" data-target="#ui_dr2"><div class="pull-left"><i class="fa fa-credit-card -setup mr-20"></i><span class="right-nav-text" style = "font-size: 11px">Businesses Payment</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
                <ul id="ui_dr2" class="collapse collapse-level-1 two-col-list">


                    <li>
                        <a href="businessCash" style = "font-size: 11px">Cash</a>
                    </li>

                    <li>
                        <a href="businessCheck" style = "font-size: 11px">Cheque</a>
                    </li>
                    <li>
                        <a href="businessTransfer" style = "font-size: 11px">Transfer</a>
                    </li>
                    <li>
                        <a href="businesstrans" style = "font-size: 11px">All Transactions</a>
                    </li>

                </ul>
            </li>




        </ul>
    </div>
    <!-- /Left Sidebar Menu -->

    <!-- Right Sidebar Menu -->

    <!-- /Right Sidebar Menu -->

    <!-- Right Setting Menu -->


    <!-- Right Sidebar Backdrop -->
    <div class="right-sidebar-backdrop"></div>
    <!-- /Right Sidebar Backdrop -->

    <!-- Main Content -->
    <div class="page-wrapper">
        <div class="container-fluid pt-25">
<!--            <div class="row" >-->
<!---->
<!---->
<!--                <div class="col-sm-2">-->
<!--                    <label class="control-label mb-10 text-left" style="font-size: 11px;">From </label>-->
<!--                    <input type="date" class="form-control" required data-required="true" id="from_date" style="font-size: 11px;">-->
<!--                </div>-->
<!--                <div class="col-sm-2">-->
<!--                    <label class="control-label mb-10 text-left" style="font-size: 11px;">To</label>-->
<!--                    <input type="date" class="form-control" required id="to_date" style="font-size: 11px;">-->
<!--                </div>-->
<!---->
<!--                <div class="col-sm-2">-->
<!--                    <button id="btndownloadpdf" class="btn btn-success  pull-right btn-xs generatedata " style = " ; background-color: forestgreen; border-color: forestgreen; margin-top: 40px"><i class="fa fa-download"></i><span class="btn-text"> Download PDF </span></button>-->
<!--                </div>-->
<!--                <div class="col-sm-2">-->
<!--                    <button id="btndownloadexcel" class="btn btn-success  pull-right btn-xs generatedatax" style = " ; background-color: forestgreen; border-color: forestgreen; margin-top: 40px"><i class="fa fa-download"></i><span class="btn-text">  Download Excel </span></button>-->
<!--                </div>-->
<!---->
<!--            </div>-->
            <br><hr>



            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-default card-view">
                        <div class="panel-heading">

                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-wrapper collapse in">
                            <div class="panel-body">
                                <div class="table-wrap" style = "font-size: 12px">
                                    <div class="">
                                        <table id="myTable1" class="table table-hover display  pb-30" style = "font-size: 12px">
                                            <thead style = "font-size: 12px">
                                            <h5> BUSINESSES TRANSACTIONS</h5>
                                            <tr style = "font-size: 12px">
                                                <th>TRANSACTION ID</th>
                                                <th>PENSION ID </th>
                                                <th>CHEQUE NUMBER </th>
                                                <th>PAYEE NAME </th>
                                                <th>PAYEE NUMBER </th>
                                                <th>AMOUNT</th>
                                                <th>BANK</th>
                                                <th>DATE/TIME</th>
                                                <th>CATEGORY</th>
                                                <th>ACTION</th>




                                            </tr>
                                            </thead>
                                            <tfoot style = "font-size: 12px">
                                            <tr style = "font-size: 12px">

                                                <th></th>
                                                <th></th>
                                                <th> </th>
                                                <th></th>

                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>



                                            </tr>
                                            </tfoot>
                                            <tbody>

                                            <?php foreach ($records as $record):?>

                                                <tr>
                                                    <td><?php echo strtoupper($record->transid); ?></td>
                                                    <td><?php echo strtoupper($record->pensionid); ?></td>
                                                    <td><?php echo strtoupper($record->chequenumber  ); ?></td>
                                                    <td><?php echo strtoupper($record->payeesname); ?></td>
                                                    <td><?php echo strtoupper($record->payeescontact); ?></td>

                                                    <td><?php echo strtoupper($record->amount); ?></td>
                                                    <td><?php echo strtoupper(bankname($record->banksortcode)); ?></td>
                                                    <td><?php echo strtoupper($record->datetrans ." " .$record->timetrans); ?></td>
                                                    <td><?php echo strtoupper($record->category ); ?></td>



                                                    <td><button  type="button" data-toggle="modal" class="updatetrans btn btn-warning  pull-middle btn-xs" data-id="<?php echo $record->id; ?>" data-pensionid="<?php echo $record->pensionid;?>" data-chequenumber="<?php echo $record->chequenumber;?>" data-payeesname="<?php echo $record->payeesname;?>" data-payeescontact="<?php  echo $record->payeescontact;?>" data-amount="<?php echo $record->amount; ?>" data-banksortcode="<?php echo $record->banksortcode;?>" data-category="<?php echo $record->category ;?>" style = " background-color: forestgreen;border-color: forestgreen"><span class="btn-text">  &nbsp  &nbsp  &nbsp  &nbsp   &nbsp &nbsp  UPDATE &nbsp  &nbsp  &nbsp &nbsp  &nbsp  &nbsp</span></button>
                                                    </td>

                                                </tr>

                                            <?php endforeach;?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Row -->

    <!-- Row -->
    <!-- Modal -->
    <div class="modal fade" id="changePassword" role="dialog">
        <div class="modal-dialog">
            <!-- <form action="post" method="USerUtilitesController/change_password"> -->
            <!-- <form> -->
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <center><h4 class="modal-title">Change Password</h4></center>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="field-1" class="control-label">Old Password</label>
                                <input type="text" id="oldpass" class="form-control" id="field-1" >
                                <input type="hidden" id="mypasswordchangemodal">
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="field-1" class="control-label">New Password</label>
                                <input type="text" id="newpass" class="form-control" id="field-1"  >
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="field-2" class="control-label">Confirm New Password</label>
                                <input type="text" id="newpassconf" class="form-control" id="field-2" >
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">

                    <center>
                        <span id="error_exist" style="color:red; "></span><br>
                        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                        <button type="submit" id="changepasssub" class="btn btn-info waves-effect waves-light"
                                style="background-color: #26428b !important; border-color:#26428b !important ">Save changes</button>
                    </center>
                </div>
            </div>
            <!--  </form> -->
        </div>

    </div>


    <div class="modal fade" id="updatecashtrans" role="dialog">
        <div class="modal-dialog">
            <form action="Payment/updatecash" method="post" data-parsley-validate="">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">UPDATE</h4>

                        <div class="form-group">
                            <div class="row">
                                <input id="id_cash" class="form-control" type="hidden" style="font-size: 11px" name="id_cash" required="">

                                <div class="col-md-6">
                                    <label  class="form-label" style="font-size: 12px">PENSION ID</label>
                                    <input id="pensionid_cash" class="form-control" type="text" style="font-size: 11px" name="pensionid_cash" required="">
                                </div>

                                <div class="col-md-6">
                                    <label  class="form-label" style="font-size: 12px">PAYEES NAME</label>

                                    <input id="payeesname_cash" class="form-control" type="text" style="font-size: 11px" name="payeesname_cash" required="">
                                </div>

                            </div>

                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label  class="form-label" style="font-size: 12px">AMOUNT:</label>
                                    <input id="amount_cash" class="form-control" type="text" style="font-size: 11px" name="amount_cash" required="">
                                </div>


                                <div class="col-md-6">
                                    <label  class="form-label" style="font-size: 12px">PAYEES CONTACT</label>

                                    <input id="payeescontact_cash" class="form-control" type="text" style="font-size: 11px" name="payeescontact_cash" required="">
                                </div>


                            </div>
                        </div>
                    </div>
                    <div class="modal-body">

                    </div>
                    <div class="modal-footer">
                        <center>

                            <button type="submit" class="btn " style = "background-color: #26428b; border-color: #26428b"><i class="icon icon-save"></i> SAVE CHANGES</button>

                        </center>
                    </div>



                </div>

        </div>
        </form>
    </div>
</div>
<div class="modal fade" id="updatechequetrans" role="dialog">
    <div class="modal-dialog">
        <form action="Payment/updatecheck" method="post" data-parsley-validate="">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">UPDATE</h4>

                    <div class="form-group">
                        <div class="row">
                            <input id="id_cheq" class="form-control" type="hidden" style="font-size: 11px" name="id_cheq" required="">

                            <div class="col-md-6">
                                <label  class="form-label" style="font-size: 12px">PENSION ID</label>
                                <input id="pensionid_cheq" class="form-control" type="text" style="font-size: 11px" name="pensionid_cheq" required="">
                            </div>

                            <div class="col-md-6">
                                <label  class="form-label" style="font-size: 12px">PAYEES NAME</label>

                                <input id="payeesname_cheq" class="form-control" type="text" style="font-size: 11px" name="payeesname_cheq" required="">
                            </div>

                        </div>

                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label  class="form-label" style="font-size: 12px">AMOUNT:</label>
                                <input id="amount_cheq" class="form-control" type="text" style="font-size: 11px" name="amount_cheq" required="">
                            </div>


                            <div class="col-md-6">
                                <label  class="form-label" style="font-size: 12px">PAYEES CONTACT</label>

                                <input id="payeescontact_cheq" class="form-control" type="text" style="font-size: 11px" name="payeescontact_cheq" required="">
                            </div>


                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label  class="form-label" style="font-size: 12px">BANK:</label>
                                <!--                                    <input id="bank_cheq" class="form-control" type="text" style="font-size: 11px" name="bank_cheq" required="">-->
                                <select id="bank_cheq" class="form-control" placeholder="click tochoose an Option"  name="bank_cheq"  required>
                                    <option value="">---</option>

                                    <?php
                                    foreach($banks as $bank){?>
                                        <option value="<?php echo $bank->sortcode;?>"><?php echo $bank->bankname;?></option>
                                    <?php }
                                    ?>


                                </select>
                            </div>
                            <div class="col-md-6">
                                <label  class="form-label" style="font-size: 12px">CHEQUE NUMBER:</label>
                                <input id="cheqnumber_cheq" class="form-control" type="text" style="font-size: 11px" name="cheqnumber_cheq" required="">
                            </div>



                        </div>
                    </div>

                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <center>

                        <button type="submit" class="btn " style = "background-color: #26428b; border-color: #26428b"><i class="icon icon-save"></i> Save</button>

                    </center>
                </div>
            </div>
        </form>
    </div>
</div>

<div class="modal fade" id="logout" role="dialog">
    <div class="modal-dialog">
        <form action="logout" method="post">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Are you sure you want to logout</h4>

                    <p></p>

                </div>

                <div class="modal-footer">
                    <center>
                        <button type="submit" class="btn btn-danger" style = "background-color: #26428b; border-color: #26428b">OK</button>
                        <button type="button" class="btn btn-danger" style = "background-color: #26428b; border-color: #26428b">Cancel</button>
                    </center>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- Row -->
<!-- Row -->
</div>

<!-- Footer -->

<!-- /Footer -->

</div>
<!-- /Main Content -->

</div>
<!-- /#wrapper -->

<!-- JavaScript -->

<!-- jQuery -->
<script src="vendors/bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>


<!-- Data table JavaScript -->
<script src="vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="vendors/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="dist/js/responsive-datatable-data.js"></script>
<!-- Slimscroll JavaScript -->
<script src="dist/js/jquery.slimscroll.js"></script>

<!-- simpleWeather JavaScaript -->
<script src="vendors/bower_components/moment/min/moment.min.js"></script>
<script src="vendors/bower_components/simpleWeather/jquery.simpleWeather.min.js"></script>
<script src="dist/js/simpleweather-data.js"></script>

<!-- Progressbar Animation JavaScript -->
<script src="vendors/bower_components/waypoints/lib/jquery.waypoints.min.js"></script>
<script src="vendors/bower_components/jquery.counterup/jquery.counterup.min.js"></script>

<!-- Fancy Dropdown JS -->
<script src="dist/js/dropdown-bootstrap-extended.js"></script>
<!-- Morris Charts JavaScript -->
<script src="vendors/bower_components/raphael/raphael.min.js"></script>
<script src="vendors/bower_components/morris.js/morris.min.js"></script>
<script src="dist/js/morris-data.js"></script>

<!-- Sparkline JavaScript -->
<script src="vendors/jquery.sparkline/dist/jquery.sparkline.min.js"></script>

<!-- Owl JavaScript -->
<script src="vendors/bower_components/owl.carousel/dist/owl.carousel.min.js"></script>

<!-- ChartJS JavaScript -->
<script src="vendors/chart.js/Chart.min.js"></script>
<!-- ChartJS JavaScript -->
<script src="vendors/chart.js/Chart.min.js"></script>
<script src="dist/js/chartjs-data.js"></script>
<!-- Morris Charts JavaScript -->
<script src="vendors/bower_components/raphael/raphael.min.js"></script>
<script src="vendors/bower_components/morris.js/morris.min.js"></script>
<script src="vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js"></script>
<script src="assets/parsley/js/parsley.min.js"></script>
<!-- Switchery JavaScript -->
<script src="vendors/bower_components/switchery/dist/switchery.min.js"></script>

<!-- Init JavaScript -->
<script src="dist/js/init.js"></script>
<script src="dist/js/dashboard-data.js"></script>
<script>
    $(document).on('click','.updatetrans',function() {
        var category=$(this).data('category');

        if(category =='CHEQUE'){
            /****CHEQUE PAYMENT*****/
            $('#updatechequetrans').modal('show');
            $('#updatecashtrans').modal('hide');
            /************send data to modal*****************/
            $('#id_cheq').val($(this).data('id'));
            $('#pensionid_cheq').val($(this).data('pensionid'));
            $('#payeesname_cheq').val($(this).data('payeesname'));
            $('#amount_cheq').val($(this).data('amount'));
            $('#payeescontact_cheq').val($(this).data('payeescontact'));
            $('#cheqnumber_cheq').val($(this).data('chequenumber'));
            $('#bank_cheq').val($(this).data('banksortcode')).change();
        }
        else if(category =='CASH'){
            /****CASH PAYMENT*****/
            $('#updatecashtrans').modal('show');
            $('#updatechequetrans').modal('hide');
            /************send data to modal*****************/
            $('#id_cash').val($(this).data('id'));
            $('#pensionid_cash').val($(this).data('pensionid'));
            $('#payeesname_cash').val($(this).data('payeesname'));
            $('#amount_cash').val($(this).data('amount'));
            $('#payeescontact_cash').val($(this).data('payeescontact'));

        }




    });
    //
    //    $(document).on('click','.generatedata',function() {
    //
    //        $('#from_date').parsley().validate();
    //        $('#to_date').parsley().validate();
    //
    //        if( ($('#from_date').parsley().isValid()) && ($('#to_date').parsley().isValid()) ) {
    //            var fromdate = $("#from_date").val();
    //            var todate = $("#to_date").val();
    //
    //            // var  location='Pdf/couponreward/' + fromdate + '/' + todate;
    //            //*changed to the the file on vroofi
    //            var  location = 'http://testvroofi.com/loyalstarpdf_excel/pdf.php?type=1&date_from='+ fromdate +'&date_to='+ todate;
    //
    //            window.location.href = location;
    //        }
    //    });
    //
    //    $(document).on('click','.generatedatax',function() {
    //
    //        $('#from_date').parsley().validate();
    //        $('#to_date').parsley().validate();
    //
    //        if( ($('#from_date').parsley().isValid()) && ($('#to_date').parsley().isValid()) ) {
    //            var fromdate = $("#from_date").val();
    //            var todate = $("#to_date").val();
    //            //var  location='Cexcel/couponreward/' + fromdate + '/' + todate
    //            //*changed to the the file on vroofi
    //            var  location = 'http://testvroofi.com/loyalstarpdf_excel/excel.php?type=1&date_from='+ fromdate +'&date_to='+ todate;
    //            window.location.href = location;
    //        }
    //    });
</script>
<script>
    $(document).on('click','#changepasssub',function(e) {
        e.preventDefault();
        var oldpass= $('#oldpass').val();
        var newpass = $('#newpass').val();
        var newpassconf = $('#newpassconf').val();
        var useremail=$('#mypasswordchangemodal').val();
        //alert(useremail);
        if(oldpass=="")
        {

        }
        else if(newpass=="")
        {

        }
        else if(newpassconf=="")
        {           }
        else{
            e.preventDefault();

            $.ajax({
                type:'POST',
                data:{oldpass:oldpass,newpass:newpass,newpassconf:newpassconf,useremail:useremail },
                url:'<?php echo site_url('User/change_password'); ?>',
                success:function(result)
                {
                    //alert(result);
                    if(result==1)
                    {
                        $('#error_exist').html("Password Change successful");
                        location.reload();
                    }
                    else if(result==2) {
                        // window
                        $('#error_exist').html("Your New password Confirmation dont Match!!");
                        //location.reload();
                    }
                    else{
                        // window
                        $('#error_exist').html("Old Password is Wrong!!");
                        //location.reload();

                    }
                }
            });
        }
    });
</script>
<script type="text/javascript">
    $(document).on('click','#mychangepass,.enableuser',function(){
        //SHOW MODAL
        $('#mypasswordchangemodal').val($(this).data('useremail'));
        var iddelete=$(this).data('usersfullname');
        $('#deleteusername').html(iddelete);


        $('#enableuserid').val($(this).data('id'));
        var iddelete=$(this).data('usersfullname');
        $('#enableusername').html(iddelete);
    });
</script>

</body>

</html>
