<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>Stanbic-Pension Contribution.</title>
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content=""/>

    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico">
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <!-- Morris Charts CSS -->
    <link href="vendors/bower_components/morris.js/morris.css" rel="stylesheet" type="text/css"/>

    <!-- Data table CSS -->

    <link href="vendors/bower_components/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
    <link href="vendors/bower_components/datatables.net-responsive/css/responsive.dataTables.min.css" rel="stylesheet" type="text/css"/>


    <link href="vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.css" rel="stylesheet" type="text/css">
    <!-- Morris Charts CSS -->
    <link href="vendors/bower_components/morris.js/morris.css" rel="stylesheet" type="text/css"/>
    <!-- Custom CSS -->
    <link href="dist/css/style.css" rel="stylesheet" type="text/css">
    <link href="assets/parsley/css/parsley.css" type="text/css">

</head>

<body>
<!-- Preloader -->
<div class="preloader-it">
    <div class="la-anim-1"></div>
</div>
<!-- /Preloader -->
<div class="wrapper theme-1-active pimary-color-red">
    <!-- Top Menu Items -->
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="mobile-only-brand pull-left">
            <div class="nav-header pull-left">
                <div class="logo-wrap">
                    <a href="index.html">
                        <img class="brand-img" src="assets/files/logo.png" alt="brand" height="40px" />
                    </a>
                </div>
            </div>
            <a id="toggle_nav_btn" class="toggle-left-nav-btn inline-block ml-20 pull-left" href="javascript:void(0);"><i class="zmdi zmdi-menu"></i></a>
            <a id="toggle_mobile_search" data-toggle="collapse" data-target="#search_form" class="mobile-only-view" href="javascript:void(0);"><i class="zmdi zmdi-search"></i></a>
            <a id="toggle_mobile_nav" class="mobile-only-view" href="javascript:void(0);"><i class="zmdi zmdi-more"></i></a>

        </div>
        <div id="mobile_only_nav" class="mobile-only-nav pull-right">
            <a href="login" >  <span style = "font-size: 10px"> Welcome, <?php echo $usersfullname;?></span> |  <span style = "font-size: 10px"><i class = "fa fa-lock"></i> Log Out</span></a>|<a href="#" data-toggle="modal" id="mychangepass" data-useremail="<?php echo $useremail;?>" data-target="#changePassword"><span style = "font-size: 10px; color: #1a79d6"><i class = "fa fa-edit"></i> Change Password</span></a>
        </div>
    </nav>
    <!-- /Top Menu Items -->

    <!-- Left Sidebar Menu -->
    <div class="fixed-sidebar-left">
        <ul class="nav navbar-nav side-nav nicescroll-bar">



            <li class="navigation-header">
                <span style="color:#26428b">BRANCH COLLECTION</span>
                <i class="zmdi zmdi-more"></i>
            </li>
            <li>
                <a  href="javascript:void(0);" data-toggle="collapse" data-target="#ui_dr1"><div class="pull-left"><i class="fa fa-money -setup mr-20"></i><span class="right-nav-text" style = "font-size: 11px">Individual Payment</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
                <ul id="ui_dr1" class="collapse collapse-level-1 two-col-list">


                    <li>
                        <a href="individualCash" style = "font-size: 11px">Cash</a>
                    </li>

                    <li>
                        <a href="individualCheck" style = "font-size: 11px">Cheque</a>
                    </li>
                    <li>
                        <a href="individualTransfer" style = "font-size: 11px">Transfer</a>
                    </li>
                    <li>
                        <a href="individualtrans" style = "font-size: 11px">All Transactions</a>
                    </li>

                </ul>
            </li>
            <li>
                <a class="active"  style="background-color:#26428b" href="javascript:void(0);" data-toggle="collapse" data-target="#ui_dr2"><div class="pull-left"><i class="fa fa-credit-card -setup mr-20"></i><span class="right-nav-text" style = "font-size: 11px">Businesses Payment</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
                <ul id="ui_dr2" class="collapse collapse-level-1 two-col-list">


                    <li>
                        <a href="businessCash" style = "font-size: 11px">Cash</a>
                    </li>

                    <li>
                        <a href="businessCheck" style = "font-size: 11px">Cheque</a>
                    </li>
                    <li>
                        <a href="businessTransfer" style = "font-size: 11px">Transfer</a>
                    </li>
                    <li>
                        <a href="businesstrans" style = "font-size: 11px">All Transactions</a>
                    </li>

                </ul>
            </li>




        </ul>
    </div>
    <!-- /Left Sidebar Menu -->

    <!-- Right Sidebar Menu -->

    <!-- /Right Sidebar Menu -->

    <!-- Right Setting Menu -->


    <!-- Right Sidebar Backdrop -->
    <div class="right-sidebar-backdrop"></div>
    <!-- /Right Sidebar Backdrop -->

    <!-- Main Content -->
    <div class="page-wrapper">
        <div class="container-fluid pt-25">


            <h6  style="color:#26428b ;float:left;">BUSINESS CASH TRANSACTION</h6>
            <br>

            <hr style="border-color: #26428b">

            <div class="form-group">
                <form id="myform">
                <div class="row">

                    <div class="col-sm-2">

                        <label class="control-label mb-10 text-left" style="font-size: 11px;">TRANSACTI0N ID</label>
                        <input  name="transid" id="transid"  class="form-control "  required ><br>

                    </div>
                    <div class="col-sm-2">

                        <label class="control-label mb-10 text-left" style="font-size: 11px;">PAYEE NAME :</label>
                        <input  name="payersName" id="payersName"  class="form-control "  required  disabled><br>

                    </div>
                    <div class="col-sm-2">

                        <label class="control-label mb-10 text-left" style="font-size: 11px;">PAYEE CONTACT NUMBER :</label>
                        <input  name="payersNumber" id="payersNumber"  class="form-control "  required disabled><br>

                    </div>
                    <div class="col-sm-2">

                        <label class="control-label mb-10 text-left" style="font-size: 11px;">CONTRIBUTER'S MONTH AND YEAR :</label>
                        <input disabled  type="month" name="" id="contr_duration"  class="form-control "  required  ><br>

                    </div>
                    <div class="col-sm-2">

                        <label  class="control-label mb-10 text-left" style="font-size: 11px;"></label>
                        <input type="hidden" name="amount" id="amount"  class="form-control "  required disabled><br>

                        <input type="hidden"  name="paymentmode" id="paymentmode"  value="<?php echo 'BUSINESS CASH'; ?>" class="form-control "  required disabled ><br>
                        <input type="hidden"  name="checknumber" id="checknumber"  value="<?php  ?>" class="form-control "  required disabled ><br>
                        <input type="hidden"  name="banksortcode" id="banksortcode"  value="<?php  ?>" class="form-control "  required disabled ><br>
                        <input type="hidden"  name="category" id="category"  value="<?php echo 'CASH'; ?>" class="form-control "  required disabled ><br>
                        <input type="hidden"  name="branch" id="branch"  value="<?php echo ''; ?>" class="form-control "  required disabled ><br>


                    </div>

                    <div class="col-sm-2">
<br>
<br>
                        <button id="submit" disabled class="btn btn-success  pull-right btn-xs generatedata " style = " background-color: forestgreen; border-color: forestgreen;"><i class="fa fa-check"></i><span class="btn-text"> MAKE PAYMENT</span></button>
                    </div>
                </div>
                    </form>
                <center><h4 id="validName" style="color: #26428b;"></h4></center>
                <hr style="color:#26428b">
                
            </div>




        </div>
    </div>
    <!-- /Row -->


    <!-- Modal -->
    <div class="modal fade" id="changePassword" role="dialog">
        <div class="modal-dialog">
            <!-- <form action="post" method="USerUtilitesController/change_password"> -->
            <!-- <form> -->
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <center><h4 class="modal-title">Change Password</h4></center>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="field-1" class="control-label">Old Password</label>
                                <input type="text" id="oldpass" class="form-control" id="field-1" >
                                <input type="hidden" id="mypasswordchangemodal">
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="field-1" class="control-label">New Password</label>
                                <input type="text" id="newpass" class="form-control" id="field-1"  >
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="field-2" class="control-label">Confirm New Password</label>
                                <input type="text" id="newpassconf" class="form-control" id="field-2" >
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">

                    <center>
                        <span id="error_exist" style="color:red; "></span><br>
                        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                        <button type="submit" id="changepasssub" class="btn btn-info waves-effect waves-light"
                                style="background-color: #26428b !important; border-color:#26428b !important ">Save changes</button>
                    </center>
                </div>
            </div>
            <!--  </form> -->
        </div>

    </div>


    <div class="modal fade" id="logout" role="dialog">
        <div class="modal-dialog">
            <form action="logout" method="post">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Are you sure you want to logout</h4>

                        <p></p>

                    </div>

                    <div class="modal-footer">
                        <center>
                            <button type="submit" class="btn btn-danger" style = "background-color: #26428b; border-color: #26428b">OK</button>
                            <button type="button" class="btn btn-danger" style = "background-color: #26428b; border-color: #26428b">Cancel</button>
                        </center>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- Row -->
</div>

<!-- Footer -->

<!-- /Footer -->

</div>
<!-- /Main Content -->

</div>
<!-- /#wrapper -->

<!-- JavaScript -->

<!-- jQuery -->
<script src="vendors/bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>


<!-- Data table JavaScript -->
<script src="vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="vendors/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="vendors/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="dist/js/responsive-datatable-data.js"></script>
<!-- Slimscroll JavaScript -->
<script src="dist/js/jquery.slimscroll.js"></script>

<!-- simpleWeather JavaScript -->
<script src="vendors/bower_components/moment/min/moment.min.js"></script>
<script src="vendors/bower_components/simpleWeather/jquery.simpleWeather.min.js"></script>
<script src="dist/js/simpleweather-data.js"></script>

<!-- Progressbar Animation JavaScript -->
<script src="vendors/bower_components/waypoints/lib/jquery.waypoints.min.js"></script>
<script src="vendors/bower_components/jquery.counterup/jquery.counterup.min.js"></script>

<!-- Fancy Dropdown JS -->
<script src="dist/js/dropdown-bootstrap-extended.js"></script>
<!-- Morris Charts JavaScript -->
<script src="vendors/bower_components/raphael/raphael.min.js"></script>
<script src="vendors/bower_components/morris.js/morris.min.js"></script>
<!--<script src="dist/js/morris-data.js"></script>-->
<!-- Sparkline JavaScript -->
<script src="vendors/jquery.sparkline/dist/jquery.sparkline.min.js"></script>

<!-- Owl JavaScript -->
<script src="vendors/bower_components/owl.carousel/dist/owl.carousel.min.js"></script>

<!-- ChartJS JavaScript -->
<script src="vendors/chart.js/Chart.min.js"></script>
<!-- ChartJS JavaScript -->
<script src="vendors/chart.js/Chart.min.js"></script>
<!--<script src="dist/js/chartjs-data.js"></script>-->

<!-- Morris Charts JavaScript -->
<script src="vendors/bower_components/raphael/raphael.min.js"></script>
<script src="vendors/bower_components/morris.js/morris.min.js"></script>
<script src="vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js"></script>

<!-- Switchery JavaScript -->
<script src="vendors/bower_components/switchery/dist/switchery.min.js"></script>

<!-- Init JavaScript -->
<script src="dist/js/init.js"></script>
<script src="dist/js/dashboard-data.js"></script>
<script src="assets/parsley/js/parsley.min.js"></script>
<script>

    $(document).on('change','#transid',function() {

        var transid=$("#transid").val();

        $.ajax({
            type:'POST',
            data:{
                transid:transid

            },
            url:'<?php echo site_url('Payment/validatebusiness'); ?>',
            beforeSend: function() {
                // setting a timeout
                $('#validName').html('<p>Verifying....</p><img src="assets/img/loader.gif"  height="" width="">');

            },
            complete: function() {
                // $('#validName').html('');
            },
            success:function(result){
                var x=result;
//                alert(x);
                if(x !='error'){
                    if( x=='0.00'){
                        $('#validName').html('<p> Invalid Transaction ID </p>');
                        $( "#payersName" ).prop( "disabled", true );
                        $( "#payersNumber" ).prop( "disabled", true );
                        $( "#amount" ).prop( "disabled", true );
                        $( "#submit" ).prop( "disabled", true );
                        $( "#contr_duration" ).prop( "disabled", true );
                    }else{
                        $('#validName').html('<p> Payment for <strong style="color:black"> '+ x +'</strong> is valid</p>');
                        $( "#payersName" ).prop( "disabled", false );
                        $( "#payersNumber" ).prop( "disabled", false );
                        $( "#contr_duration" ).prop( "disabled", false );
                        $( "#amount" ).prop( "disabled", false );
                        $( "#submit" ).prop( "disabled", false );
                        
                    }

                }else{
                    $('#validName').html('<p> PensionID could <strong style="color:black"> not</strong> be validated</p>');
                }
            }
        });
    });


</script>
<script>

    $(document).on('click','#submit',function(e) {
        e.preventDefault();
        $('#myform').parsley().validate();
        if($('#myform').parsley().isValid()){

            var transid = $('#transid').val();
            var payersname = $('#payersName').val();
            var payersnumber = $('#payersNumber').val();
            var amount = $('#amount').val();
            var paymentmode = $('#paymentmode').val();
            var checknumber = $('#checknumber').val();
            var banksortcode = $('#banksortcode').val();
            var category = $('#category').val();
            var branch = $('#branch').val();
            var contr_duration = $('#contr_duration').val();


            $.ajax({
                type:'POST',
                data:{
                    transid:transid,
                    payersname:payersname,
                    payersnumber:payersnumber,
                    amount:amount,
                    transid:transid,
                    paymentmode:paymentmode,
                    checknumber:checknumber,
                    banksortcode:banksortcode,
                    category:category,
                    branch:branch,
                    contr_duration:contr_duration

                },
                url:'<?php echo site_url('Payment/updatecashbusiness'); ?>',

                beforeSend: function() {
                    // setting a timeout
                    $('#validName').html('processing....');
                    $( "#submit" ).prop( "disabled", true );
                },
                complete: function() {
//                    $('#validName').html('');

                },
                success:function(result){

                    if(result!='error'){
                        var x=result.split("|");
                        // alert(x[1]);
                        $('#validName').html('<div class="alert alert-dismissable" style="background-color: #26428b;"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong>Recorded Successfully!</strong> A reference id :'+ x[1] +'. </div>');

                    }else{
                        $('#validName').html('<div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong>Error!</strong>  </div>');

                    }
                }
            });





        }


    });

</script>
<script>
    $(document).on('click','#changepasssub',function(e) {
        e.preventDefault();
        var oldpass= $('#oldpass').val();
        var newpass = $('#newpass').val();
        var newpassconf = $('#newpassconf').val();
        var useremail=$('#mypasswordchangemodal').val();
        //alert(useremail);
        if(oldpass=="")
        {

        }
        else if(newpass=="")
        {

        }
        else if(newpassconf=="")
        {           }
        else{
            e.preventDefault();

            $.ajax({
                type:'POST',
                data:{oldpass:oldpass,newpass:newpass,newpassconf:newpassconf,useremail:useremail },
                url:'<?php echo site_url('User/change_password'); ?>',
                success:function(result)
                {
                    //alert(result);
                    if(result==1)
                    {
                        $('#error_exist').html("Password Change successful");
                        location.reload();
                    }
                    else if(result==2) {
                        // window
                        $('#error_exist').html("Your New password Confirmation dont Match!!");
                        //location.reload();
                    }
                    else{
                        // window
                        $('#error_exist').html("Old Password is Wrong!!");
                        //location.reload();

                    }
                }
            });
        }
    });
</script>
<script type="text/javascript">
    $(document).on('click','#mychangepass,.enableuser',function(){
        //SHOW MODAL
        $('#mypasswordchangemodal').val($(this).data('useremail'));
        var iddelete=$(this).data('usersfullname');
        $('#deleteusername').html(iddelete);


        $('#enableuserid').val($(this).data('id'));
        var iddelete=$(this).data('usersfullname');
        $('#enableusername').html(iddelete);
    });
</script>
</body>

</html>
