<!DOCTYPE html>
<!-- saved from url=(0019)https://small.chat/ -->
<html lang="en" class="fa-events-icons-ready"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Stanbic-Pension Contribution.</title>



    <meta name="keywords" content="">
    <meta name="author" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <link rel="shortcut icon" href="assets/files/favicon.ico">

    <meta property="og:title" content="">
    <meta property="og:url" content="">
    <meta property="og:type" content="website">
    <meta property="og:description" content="">
    <meta property="og:image" content="">

    <meta property="og:image:width" content="800">
    <!-- <meta property="og:image:height" content="800" /> -->

    <meta name="twitter:card" content="">
    <meta name="twitter:site" content="">
    <meta name="twitter:title" content="">
    <meta name="twitter:description" content="">
    <meta name="twitter:image" content="">


    <link href="assets/files/ae8587a781.css" media="all" rel="stylesheet">
    <link href="assets/files/css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="assets/files/beta-banners.css">
    <link rel="stylesheet" type="text/css" href="assets/files/mailchimp.css">
    <link rel="stylesheet" type="text/css" href="assets/files/style.css">
    <!-- <link rel="stylesheet" type="text/css" href="style.min.css"> -->
    <link href="assets/parsley/css/parsley.css" rel="stylesheet" type="text/css">


</head>





<header>
    <div style = "background-color:#26428b; height: 20px ">
        &nbsp
    </div>
    <div class="header_nav">
        <a href="#"><img src="assets/files/logo.png"></a>
        <div class="header_links">

            <div class="slack_button_container" style="height: 90px;">
                <a href="http://184.168.147.165:81/bulkpayment/" style="margin-top: 10px;">
                    <img alt="Add to Slack" src="assets/files/client.png" >
                </a>
            </div>

        </div>
    </div>
</header><br><br>

<div class="container">
    <section class="homepage">
        <div class="homepage_content">
            <img class="homepage_globe" src="assets/files/back.png" style="margin-top: -40px; margin-left: -20px">
            <h1 class="homepage_title" style = "color:#26428b ">Take control of <br>your retirement future.</h1>
            <p class="homepage_subtitle">pay your pension contributions, anywhere with ease anytime..</p>
<!--            <div class="slack_button_container" style="height: 90px;">-->
<!--                <a href="#" style="margin-top: 10px;">-->
<!--                    <img alt="Add to Slack" src="assets/files/add-to-slack.png" >-->
<!--                </a>-->
<!--            </div>-->
        </div>
        <div class="homepage_chat" style="margin-top: -20px">
            <img id="chat-one" src="assets/files/chat-one.png" style="width: 88%; opacity: 1; display: inline-block; transform: translateX(0px);" class="">
            <img id="chat-two" src="assets/files/chat-two.png" style="width: 80%; opacity: 1; display: inline-block; transform: translateX(-10px);" class="">
            <img id="chat-three" src="assets/files/chat-three.png" style="width: 80%; opacity: 1; display: inline-block; transform: translateX(0px);" class="">
            <img id="chat-four" src="assets/files/chat-four.png" style="width: 80%; opacity: 1; display: inline-block; transform: translateX(-10px);" class="">
        </div>
    </section>



    <footer>
        <div class="footer">

            <div class="footer_info">

                <center style = "margin-top: -30px"><img src="assets/files/paymentbanner.png"><span style="font-size: 11px;">Terms &amp; Conditions Apply | © Stanbic Bank Ghana, </span></h6>
                </center>
                <h6 >
                    <div class="legal">
                        <p><h6 style="font-size: 11px"></h6></p>
                    </div>
            </div>

            <!-- Begin MailChimp Signup Form -->
            <div id="mc_embed_signup">
                <div class="alert alert-danger" style="color:red; font-size: 11px;">
                     <?php echo $this->session->flashdata('errormessage');?>
                </div>

                <form action="fetchinfo" method="post" data-parsley-validate="">
                    <div id="mc_embed_signup_scroll">



                        <div class="mc-row-one">
                            <div class="mc-field-group mc-name">
                                <label for="mce-NAME">Pension ID#</label>
                                <input type="text" value="" name="pensionNumber" class="required" id="pensionNumber" style="width: 110%" required>
                            </div>
                            <div class="mc-field-group" style="width: 70%">
                                <label for="mce-COMPANY">Phone Number</label>
                                <input type="text"  name="phoneNumber" class="required" id="phoneNumber" required pattern="^\d{3}\d{3}\d{4}$">
                            </div>
                        </div>


                        <div class="mc-row-three">
                            <div class="mc-field-group mc-name">
                                <label for="mce-NAME">Payee's Name</label>
                                <input type="text" name="payeesname" class="required" id="mce-" style="width: 110%" required>
                            </div>
                            <div class="mc-field-group" style="width: 70%">
                                <label for="mce-COMPANY">Contributor's Month and Year</label>
                                <input type="month" value="" name="contributerMonth" class="required" id="mce-">
                            </div>
                        </div>
                       

                        <br>
                        <div class="clear">

                            <button type="submit"><img alt="Add to Slack" src="assets/files/pay.png" ></button>
<!--                            <div class="slack_button_container" style="height: 90px;">-->
<!--<!--                                <a style="margin-top: 10px;" type="submit" >-->
<!--                                <button type="submit">-->
<!--                                    <img alt="Add to Slack" src="assets/files/pay.png" >-->
<!--                                </button>-->
<!--                            </div>-->
                        </div>
                    </div>
                </form>
            </div>
            <!--End mc_embed_signup-->
        </div>
    </footer>
</div>



<script src="assets/files/jquery.min.js.download"></script>


<!-- PARALAX -->
<script type="text/javascript" src="assets/files/rellax.min.js.download"></script>
<script type="text/javascript" src="assets/files/jquery-1.12.0.min.js.download"></script>
<script type="text/javascript" src="assets/files/velocity.min.js.download"></script>
<script type="text/javascript" src="assets/files/velocity.ui.js.download"></script>
<script type="text/javascript" src="assets/files/smallchat.js.download"></script>
<script src="assets/parsley/js/parsley.min.js"></script>
<script>
    var rellax = new Rellax('.rellax', {
        // center: true
    });
</script>



</body></html>